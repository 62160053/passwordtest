const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 caharacter to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 caharacter to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 caharacter to be false', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 caharacter to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('test Digit', () => {
  test('should has Digit 0 in password', () => {
    expect(checkDigit('0')).toBe(true)
  })

  test('should has Digit 9 in password', () => {
    expect(checkDigit('9')).toBe(true)
  })

  test('should has not Digit in password', () => {
    expect(checkDigit('AAAA')).toBe(false)
  })
})

describe('test Symbol', () => {
  test('should has Digit ! in password to be true', () => {
    expect(checkSymbol('!')).toBe(true)
  })

  test('should has Digit * in password to be true', () => {
    expect(checkSymbol('*')).toBe(true)
  })

  test('should has not Digit in password to be false', () => {
    expect(checkSymbol('a')).toBe(false)
  })
})

describe('Test passwprd', () => {
  test('should password abc#123 to be true', () => {
    expect(checkPassword('abc#1234')).toBe(true)
  })

  test('should password abc#123 to be false', () => {
    expect(checkPassword('abc#123')).toBe(false)
  })
})
